package testdata

import com.monzoprep.library_domain_repository_room.model.UserServiceRoom

class UserServiceRoomTestDataBuilder {
    companion object {
        fun aUserServiceRoom(
            userServiceTypeOrdinal: Int = 0,
            serviceUuid: String = "uuid"
        ) = UserServiceRoom(userServiceTypeOrdinal, serviceUuid)
    }
}