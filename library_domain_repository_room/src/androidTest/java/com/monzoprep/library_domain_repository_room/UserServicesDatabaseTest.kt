package com.monzoprep.library_domain_repository_room

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.monzoprep.domain.models.UserServiceType
import com.monzoprep.library_domain_repository_room.dao.UserServicesDao
import com.monzoprep.library_domain_repository_room.database.UserServicesDatabase
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.not
import org.junit.After
import org.junit.Assert.assertNull
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import testdata.UserServiceRoomTestDataBuilder.Companion.aUserServiceRoom

@RunWith(AndroidJUnit4::class)
class UserServicesDatabaseTest {

    private lateinit var userServicesDao: UserServicesDao
    private lateinit var db: UserServicesDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()

        db = Room.inMemoryDatabaseBuilder(
            context,
            UserServicesDatabase::class.java
        ).build()

        userServicesDao = db.userServicesDao()
    }

    @Test
    fun shouldInsertUserService() {
        runBlocking {
            // Arrange
            val serviceType = UserServiceType.MEDICAL_INSURANCE.ordinal
            val userServiceRoom = aUserServiceRoom(serviceType)

            // Act
            userServicesDao.insertUserService(userServiceRoom)

            // Assert
            val loadedUserService = userServicesDao.loadUserService(serviceType)
            assertThat(userServiceRoom, equalTo(loadedUserService))
        }
    }

    @Test
    fun shouldUpdateUserService() {
        runBlocking {
            // Arrange
            val userServiceType = UserServiceType.MEDICAL_INSURANCE.ordinal
            val userServiceRoom = aUserServiceRoom(userServiceType)
            val updatedServiceRoom = userServiceRoom.copy(userServiceUuid = "newUuid")

            // Act
            userServicesDao.insertUserService(userServiceRoom)
            userServicesDao.updateUserService(updatedServiceRoom)
            val loadedUserService = userServicesDao.loadUserService(userServiceType)

            // Assert
            assertThat(loadedUserService, not(userServiceRoom))
            assertThat(loadedUserService, equalTo(updatedServiceRoom))
        }
    }

    @Test
    fun shouldReturnNullWhenNoUserServiceExists() {
        runBlocking {
            // Arrange
            val userServiceType = UserServiceType.MEDICAL_INSURANCE.ordinal

            // Act
            val loadedUserService = userServicesDao.loadUserService(userServiceType)

            // Assert
            assertNull(loadedUserService)
        }
    }

    @After
    fun closeDb() {
        db.close()
    }
}