package com.monzoprep.library_domain_repository_room

import com.monzoprep.domain.UserServiceTestDataBuilder.Companion.aUserService
import com.monzoprep.domain.models.UserServiceType
import com.monzoprep.library_domain_repository_room.dao.UserServicesDao
import com.monzoprep.library_domain_repository_room.mapper.UserServiceMapper
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test
import testdata.UserServiceRoomTestDataBuilder.Companion.aUserServiceRoom

class UserAssignedServicesRepositoryRoomTest {

    private val userServicesDao = mock<UserServicesDao>()
    private val userServiceMapper = UserServiceMapper()

    private val repository = UserAssignedServicesRepositoryRoom(
        userServicesDao, userServiceMapper
    )

    @Test
    fun `should return UserService with correct UserServiceType`() {
        runBlocking {
            // Arrange
            val userServiceType = UserServiceType.MEDICAL_INSURANCE
            val userServiceRoom = aUserServiceRoom(userServiceType.ordinal)
            given(userServicesDao.loadUserService(any())).willReturn(userServiceRoom)

            // Act
            val userService = repository.getService(userServiceType)

            // Assert
            assertEquals(userServiceType, userService.userServiceType)
        }
    }

    @Test
    fun `should persist valid UserService when service enabled`() {
        runBlocking {
            // Arrange
            val userService = aUserService()
            val userServiceRoom = aUserServiceRoom(userService.userServiceType.ordinal)
            given(userServicesDao.loadUserService(any())).willReturn(userServiceRoom)

            // Act
            repository.enableService(userService)
            val updatedUserService = repository.getService(userService.userServiceType)

            // Assert
            assertEquals(updatedUserService.serviceUuid, userService.serviceUuid)
            assertEquals(updatedUserService.userServiceType, userService.userServiceType)
        }
    }

    @Test
    fun `should persist empty UserService when service disabled`() {
        runBlocking {
            // Arrange
            val userService = aUserService()
            val userServiceRoom = aUserServiceRoom(userService.userServiceType.ordinal, "")
            given(userServicesDao.loadUserService(any())).willReturn(userServiceRoom)

            // Act
            repository.disableService(userService)
            val updatedUserService = repository.getService(userService.userServiceType)

            // Assert
            assertEquals(updatedUserService.serviceUuid, "")
            assertEquals(updatedUserService.userServiceType, userService.userServiceType)
        }
    }

    @Test(expected = IllegalArgumentException::class)
    fun `should throw exception when mapping inexistant UserServiceRoom`() {
        runBlocking {
            // Arrange
            val userService = aUserService()
            given(userServicesDao.loadUserService(any())).willReturn(null)

            // Act
            repository.getService(userService.userServiceType)
        }
    }
}