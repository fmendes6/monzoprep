package com.monzoprep.library_domain_repository_room

import com.monzoprep.library_domain_repository_room.mapper.UserServiceMapper
import org.junit.Assert.assertEquals
import org.junit.Test
import testdata.UserServiceRoomTestDataBuilder.Companion.aUserServiceRoom

class UserServiceMapperTest {

    private val userServiceMapper = UserServiceMapper()

    @Test
    fun `should map from room to domain model`() {
        // Arrange
        val userServiceRoom = aUserServiceRoom()

        // Act
        val userService = userServiceMapper.map(userServiceRoom)

        // Assert
        assertEquals(userService.userServiceType.ordinal, userServiceRoom.userServiceType)
        assertEquals(userService.serviceUuid, userServiceRoom.userServiceUuid)
    }
}