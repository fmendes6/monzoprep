package com.monzoprep.library_domain_repository_room

import com.monzoprep.domain.models.UserService
import com.monzoprep.domain.models.UserServiceType
import com.monzoprep.library_domain_repository.UserAssignedServicesRepository
import com.monzoprep.library_domain_repository_room.dao.UserServicesDao
import com.monzoprep.library_domain_repository_room.mapper.UserServiceMapper
import com.monzoprep.library_domain_repository_room.model.UserServiceRoom

class UserAssignedServicesRepositoryRoom(
    private val userServicesDao: UserServicesDao,
    private val userServiceMapper: UserServiceMapper
) : UserAssignedServicesRepository {

    override suspend fun getService(userServiceType: UserServiceType): UserService {
        val userServiceRoom = userServicesDao.loadUserService(userServiceType.ordinal)
        return userServiceMapper.map(userServiceRoom)
    }

    override suspend fun enableService(userService: UserService) {
        val userServiceRoom = UserServiceRoom(userService.userServiceType.ordinal, userService.serviceUuid)
        userServicesDao.updateUserService(userServiceRoom)
    }

    override suspend fun disableService(userService: UserService) {
        val userServiceRoom = UserServiceRoom(userService.userServiceType.ordinal, "")
        userServicesDao.updateUserService(userServiceRoom)
    }
}