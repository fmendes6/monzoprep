package com.monzoprep.library_domain_repository_room.mapper

import com.monzoprep.domain.models.UserService
import com.monzoprep.domain.models.UserServiceType
import com.monzoprep.library_domain_repository_room.model.UserServiceRoom

class UserServiceMapper {

    fun map(userServiceRoom: UserServiceRoom): UserService {
        return UserService(UserServiceType.values()[userServiceRoom.userServiceType], userServiceRoom.userServiceUuid)
    }
}