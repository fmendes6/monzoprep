package com.monzoprep.library_domain_repository_room.database

import android.content.Context

class UserServicesDatabaseWrapper(val context: Context) {
    fun getUserServicesDao() = UserServicesDatabase.getInstance(context).userServicesDao()
}