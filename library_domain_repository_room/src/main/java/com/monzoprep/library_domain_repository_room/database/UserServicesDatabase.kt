package com.monzoprep.library_domain_repository_room.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.monzoprep.domain.models.UserServiceType
import com.monzoprep.library_domain_repository_room.dao.UserServicesDao
import com.monzoprep.library_domain_repository_room.model.UserServiceRoom
import java.util.concurrent.Executors

@Database(entities = [UserServiceRoom::class], version = 1, exportSchema = false)
internal abstract class UserServicesDatabase : RoomDatabase() {
    abstract fun userServicesDao(): UserServicesDao

    companion object {

        @Volatile
        private var INSTANCE: UserServicesDatabase? = null
        private const val DATABASENAME = "database-user-services"

        fun getInstance(context: Context): UserServicesDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                UserServicesDatabase::class.java,
                DATABASENAME
            ).addCallback(object : Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
                    insertDataOnDatabaseCreation(context)
                }
            }).build()

        private fun insertDataOnDatabaseCreation(context: Context) {
            // insert the data on the IO Thread
            Executors.newSingleThreadExecutor().execute {
                getInstance(context)
                    .userServicesDao()
                    .insertUserService(baseData)
            }
        }

        private val baseData = UserServiceRoom(UserServiceType.MEDICAL_INSURANCE.ordinal, "")
    }
}