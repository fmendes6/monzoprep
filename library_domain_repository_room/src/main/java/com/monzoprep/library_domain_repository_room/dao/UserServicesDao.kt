package com.monzoprep.library_domain_repository_room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.monzoprep.library_domain_repository_room.model.UserServiceRoom

@Dao
interface UserServicesDao {

    @Query("SELECT * FROM UserServiceRoom WHERE user_service = (:serviceType) LIMIT 1")
    suspend fun loadUserService(serviceType: Int): UserServiceRoom

    @Update
    suspend fun updateUserService(userServiceRoom: UserServiceRoom)

    @Insert
    fun insertUserService(userServiceRoom: UserServiceRoom)
}
