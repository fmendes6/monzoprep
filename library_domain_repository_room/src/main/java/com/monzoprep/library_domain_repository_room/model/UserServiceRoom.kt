package com.monzoprep.library_domain_repository_room.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class UserServiceRoom(
    @PrimaryKey
    @ColumnInfo(name = "user_service") val userServiceType: Int,
    @ColumnInfo(name = "user_service_uuid") val userServiceUuid: String
)