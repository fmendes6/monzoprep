# MonzoPrep

[![pipeline status](https://gitlab.com/fmendes6/monzoprep/badges/master/pipeline.svg)](https://gitlab.com/fmendes6/monzoprep/commits/master)

***UPDATE - This project started as an exercise to prepare for an interview at Monzo and to which I got rejected.
Since I learnt a lot throughout the development process, I decided to keep the repository open and keep working on it. If you have any comments/suggestions, get involved by opening issues or merge requests!***

This repository has the following goals:
1. To document an audit to the current live monzo app
2. To develop a side project with a simple feature to showcase the thought and development processes while using most of the tech stack mentioned in the monzo job description.
3. Keep adding new features in order to explore how the current architecture would adapt.

To learn more about this project, please start with the [wiki](https://gitlab.com/fmendes6/monzoprep/wikis/home) but do not forget to look into the [RFC's](https://gitlab.com/fmendes6/monzoprep/tree/master/rfc), the [Merge Requests](https://gitlab.com/fmendes6/monzoprep/merge_requests?scope=all&utf8=✓&state=merged) and the final [retrospective.](https://gitlab.com/fmendes6/monzoprep/blob/master/retrospective/2019-08-project_retro.md)

## Building

#### Run the app:

1. Make sure you have [android SDK](https://developer.android.com/studio) installed 
in your machine and then [set the system variables](https://stackoverflow.com/a/53245843)
to point to the android sdk installation folder.
2. Run the following command on the terminal, from the root folder of this project:
   * `$ ./gradlew clean installDebug`

#### Run the instrumentation tests:

1. Connect a phone to your machine
2. Run the following command on the terminal:
   * `$ ./gradlew cAT`

## Technologies and Concepts used:
* Module by feature
* Coordinator pattern
* Navigator pattern
* MVVM pattern
* Repository pattern
* Dagger 2
* LiveData
* RxJava
* Coroutines
* Room

## Author
Developed by [Filipe Mendes](http://www.fmendes6.com) (follow me on [Twitter](https://www.twitter.com/fmendes6))

## Credits
* Some monzo resources have been downloaded from the monzo website or screenshotted from the live app (ex:world map)
* Some icons were downloaded from - https://material.io/resources/icons/
* All the 3rd-party library developers
* Novoda for the Issue and PR templates
* The great android community

## License
```
The MIT License (MIT)

Copyright (c) 2019 Filipe Mendes

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```
