package com.monzoprep.library_domain_usecases

import com.monzoprep.domain.UserServiceTestDataBuilder.Companion.aUserService
import com.monzoprep.domain.models.UserServiceType
import com.monzoprep.library_domain_repository.UserAssignedServicesRepository
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test

class GetUserAssignedServiceTest {

    private val userAssignedServiceRepository = mock<UserAssignedServicesRepository>()
    private val getUserAssignedService = GetUserAssignedService(userAssignedServiceRepository)

    @Test
    fun `should get user assigned service`() {
        runBlocking {
            // Arrange
            val userServiceType = UserServiceType.MEDICAL_INSURANCE
            val userService = aUserService(userServiceType = userServiceType)

            given(userAssignedServiceRepository.getService(userServiceType)).willReturn(userService)

            // Act
            val service = getUserAssignedService.get(userServiceType)

            // Assert
            verify(userAssignedServiceRepository).getService(userServiceType)
            assertEquals(service, userService)
        }
    }
}