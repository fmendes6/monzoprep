package com.monzoprep.library_domain_usecases

import com.monzoprep.domain.UserServiceTestDataBuilder.Companion.aUserService
import com.monzoprep.library_domain_repository.UserAssignedServicesRepository
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.runBlocking
import org.junit.Test

class SubscribeUserAssignedServiceTest {

    private val userAssignedServiceRepository = mock<UserAssignedServicesRepository>()
    private val subscribeUserAssignedService = SubscribeUserAssignedService(userAssignedServiceRepository)

    @Test
    fun `should invoke repository in use case`() {
        runBlocking {
            // Arrange
            val userService = aUserService()

            // Act
            subscribeUserAssignedService.subscribe(userService)

            // Assert
            verify(userAssignedServiceRepository).enableService(userService)
        }
    }
}