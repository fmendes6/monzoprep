package com.monzoprep.library_domain_usecases

import com.monzoprep.domain.models.UserService
import com.monzoprep.domain.models.UserServiceType
import com.monzoprep.library_domain_repository.UserAssignedServicesRepository

class GetUserAssignedService(
    private val userAssignedServicesRepository: UserAssignedServicesRepository
) {

    suspend fun get(userService: UserServiceType): UserService {
        return userAssignedServicesRepository.getService(userService)
    }
}