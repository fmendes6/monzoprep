package com.monzoprep.library_domain_usecases

import com.monzoprep.domain.models.UserService
import com.monzoprep.library_domain_repository.UserAssignedServicesRepository

class SubscribeUserAssignedService(
    private val userAssignedServicesRepository: UserAssignedServicesRepository
) {

    suspend fun subscribe(userService: UserService) {
        return userAssignedServicesRepository.enableService(userService)
    }
}