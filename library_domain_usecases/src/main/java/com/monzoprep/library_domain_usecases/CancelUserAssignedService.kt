package com.monzoprep.library_domain_usecases

import com.monzoprep.domain.models.UserService
import com.monzoprep.library_domain_repository.UserAssignedServicesRepository

class CancelUserAssignedService(
    private val userAssignedServicesRepository: UserAssignedServicesRepository
) {

    suspend fun cancel(userService: UserService) {
        return userAssignedServicesRepository.disableService(userService)
    }
}