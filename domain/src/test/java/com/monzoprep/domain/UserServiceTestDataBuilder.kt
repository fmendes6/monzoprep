package com.monzoprep.domain

import com.monzoprep.domain.models.UserService
import com.monzoprep.domain.models.UserServiceType
import org.jetbrains.annotations.TestOnly

class UserServiceTestDataBuilder {
    companion object {
        @TestOnly
        fun aUserService(
            userServiceType: UserServiceType = UserServiceType.MEDICAL_INSURANCE,
            serviceUuid: String = "uuid"
        ) = UserService(userServiceType, serviceUuid)
    }
}