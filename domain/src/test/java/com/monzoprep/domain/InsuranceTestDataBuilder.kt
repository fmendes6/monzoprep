package com.monzoprep.domain

import com.monzoprep.domain.models.insurance.Insurance
import com.monzoprep.domain.models.insurance.InsuranceRegion
import com.monzoprep.domain.models.insurance.InsuranceType
import org.jetbrains.annotations.TestOnly
import java.math.BigDecimal

class InsuranceTestDataBuilder {
    companion object {
        @TestOnly
        fun aInsurance(
            uuid: String = "uuid",
            providedBy: String = "monzo",
            insuranceType: InsuranceType = InsuranceType.PayPerDay,
            cost: BigDecimal = BigDecimal("1.25"),
            benefit1: String = "benefit1",
            benefit2: String = "benefit2",
            benefit3: String = "benefit3",
            region: InsuranceRegion = InsuranceRegion.Europe
        ) = Insurance(
            uuid,
            providedBy,
            insuranceType,
            cost,
            benefit1,
            benefit2,
            benefit3,
            region
        )
    }
}