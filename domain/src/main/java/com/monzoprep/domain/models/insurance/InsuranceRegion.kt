package com.monzoprep.domain.models.insurance

enum class InsuranceRegion {
    Europe,
    NorthAmerica,
    RestOfWorld
}