package com.monzoprep.domain.models.insurance

import java.math.BigDecimal

data class Insurance(
    val uuid: String,
    val providedBy: String,
    val insuranceType: InsuranceType,
    val cost: BigDecimal,
    val benefit1: String,
    val benefit2: String,
    val benefit3: String,
    val region: InsuranceRegion
)