package com.monzoprep.domain.models

class UserService(
    val userServiceType: UserServiceType,
    val serviceUuid: String
)

enum class UserServiceType {
    MEDICAL_INSURANCE
}