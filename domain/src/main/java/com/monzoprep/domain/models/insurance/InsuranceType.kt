package com.monzoprep.domain.models.insurance

enum class InsuranceType {
    Monthly,
    PayPerDay
}