package com.monzoprep

import android.app.Application
import com.monzoprep.core.di.CoreComponent
import com.monzoprep.core.di.CoreComponentProvider
import com.monzoprep.core.di.CoreModule
import com.monzoprep.core.di.DaggerCoreComponent

class MonzoApp : Application(), CoreComponentProvider {

    private lateinit var coreComponent: CoreComponent

    override fun provideCoreComponent(): CoreComponent {
        if (!this::coreComponent.isInitialized) {
            coreComponent = DaggerCoreComponent
                .builder()
                .coreModule(CoreModule(this))
                .build()
        }
        return coreComponent
    }
}