package com.monzoprep

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.monzoprep.core.navigation.NavigationActions

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        openActivity(this, NavigationActions.Dashboard)
        finish()
    }

    private fun openActivity(activity: Activity, action: String) {
        val intent = Intent(action)
        intent.setPackage(activity.packageName)
        activity.startActivity(intent)
    }
}
