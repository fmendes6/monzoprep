package com.monzoprep.core.di

import javax.inject.Scope

@Scope
@Retention
annotation class FeatureScope