package com.monzoprep.core.di

import android.content.Context
import dagger.Component
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Component(modules = [CoreModule::class])
@Singleton
interface CoreComponent {
    fun getContext(): Context
}

@Module
class CoreModule(private val context: Context) {
    @Provides
    @Singleton
    fun provideContext(): Context = context
}