package com.monzoprep.core.di

interface CoreComponentProvider {
    fun provideCoreComponent(): CoreComponent
}