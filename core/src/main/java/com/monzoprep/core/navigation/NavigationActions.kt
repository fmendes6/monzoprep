package com.monzoprep.core.navigation

object NavigationActions {
    const val Dashboard = "com.monzoprep.dashboard.open"
    const val Travel = "com.monzoprep.travel.open"
    const val InsuranceChooser = "com.monzoprep.insurance.choose.open"
    const val InsuranceDetail = "com.monzoprep.insurance.detail.open"
}