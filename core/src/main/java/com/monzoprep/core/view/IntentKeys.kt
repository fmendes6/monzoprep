package com.monzoprep.core.view

object IntentKeys {
    const val InsuranceDetailInsuraceId = "IntentKeys::InsuranceDetailInsuraceId"
    const val InsuranceDetailIsInsuraceEnabled = "IntentKeys::InsuranceDetailIsInsuraceEnabled"
}