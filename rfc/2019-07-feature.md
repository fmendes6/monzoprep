Date: 21/07/2019
* Author: Filipe Mendes

## Summary
Defining the guidelines for implementing a feature

## Motivation
Creating a base architecture/structure for each feature and the way it 
should interact and depend on other dependencies (libraries).

## Proposal

Each ':feature' will implement only the user interface code necessary 
for the user interaction while depending on (at least) a repository 
interface for its data retrieval. 

The repository will be implemented by (at least) another library that 
will do the concrete implementation of a specific data source, which 
could be a network service, a cache (in-memory/database) or even a file 
reader.

Isolated from all the application code, UI or libraries, will be a small
':domain' library containing all the domain object models to be shared 
among all the different modules. This helps creating a unified domain 
model that libraries can map to and make use of.

```
                                                       :library-network
                                                       +---------------+
                            +------------------------->|               |
                            |                          |               |
                            |                          +-------+-------+
:domain           :feature  |        :library-repository       |
+-----------+     +---------+---+    +---------------+         |
|           |<----|             |--->|               |<--------+
|           |     |             |    |               |<--------+
+-----------+     +---------+---+    +---------------+         |
                            |                                  |
                            |                          +-------+-------+
                            +------------------------->|               |
                                                       |               |
                                                       +---------------+
                                                       :library-cache

```

All the interactions between ':feature' and its libraries will be done 
entirely via interfaces and not through its concrete classes. As such, 
the only reason why ':feature' has dependencies on ':*-network' and 
':*-cache' is for initialization purposes on a configuration file of its
dependencies.

Centralizing the configuration on the feature module, helps keeping the 
libraries clean of inversion of control frameworks and makes it easy to 
swap them for different libraries at the feature level.

## Alternatives

Grouping all the different data sources of the same feature under a 
single module would simplify the dependencies between the modules but 
would require a refactor and rebuild of the `:repository` module after 
replacing one of the data sources.

```

                                     :library-repository
                                     +---------------------+
                                     |   :network          |
                                     |   +---------------+ |
                                     |   |               | |
:domain           :feature           |   |               | |
+-----------+     +-------------+    |   +---------------+ |
|           +<----+             +--->+                     |
|           |     |             |    |   :cache            |
+-----------+     +-------------+    |   +---------------+ |
                                     |   |               | |
                                     |   |               | |
                                     |   +---------------+ |
                                     |                     |
                                     +---------------------+

```
