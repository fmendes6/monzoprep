Date: 20/07/2019
* Author: Filipe Mendes

## Summary
Defining an architecture for an application modularized by feature

## Motivation
To develop a project with the goal of being as close as possible to what
the author thinks might be the architecture of the current monzo android
application (based on the job offer), while taking the time to learn 
about some concepts and technologies like:
* Modularization by feature;
* Navigator and Coordinator pattern;
* MVP vs MVVM
* Coroutines

## Proposal
Architecting an application by features has (among many others) the 
following advantages:
- Faster builds;
- Helps scaling the team to work on new features without the need to 
touch a lot of legacy code;
- It potentially reduces the amount of long merge conflicts as a result 
of independent development;
- The ability to experiment new technologies in isolated modules.

#### Architecture
This means that all the features should be independent and decoupled 
from each other and in order to achieve that, the main application 
module should be interpreted as a wrapper of all the features of an app.

```
:app
+-------------------------+
|                         |
+---+-----------------+---+
    |                 |
    v                 v
:featureX         :featureY
+-------+         +-------+
|       |         |       |
+---+---+         +-------+
    |
    v
:library
+-------+
|       |
+-------+
```

Each feature is a module containing only UI code (`Activity`, 
`Fragment`, `Viewmodel`, etc) and will depend on any libraries necessary
(ex:network, cache, etc.) without extending that dependency to the app 
module.

Contrary to the feature modules, library modules are allowed to depend 
on other library modules. An example of this would be when library A 
needs to access a data sources that belongs to another feature. 


#### Navigation

A feature can be composed of several screens and navigating between 
those is nothing more than starting an explicit intent to that screen, 
since it is contained in the same module. 

However, in order to navigate between screens of different feature 
modules, it will be necessary to take advantage of the android intent 
system, in which the operating system resolves the right screen to 
navigate to at runtime based on a `Action` of the implicit intent that 
the app is about to launch.

```
fun openActivity(activity, action) {
    val intent = Intent(action)
    activity.startActivity(intent)
}
```
This will require (at least) all the activities that are "launchable" 
from different modules, to have an `Action` associated in its feature 
module manifest declaration.

##### a) Navigation Logic

In an attempt to isolate the navigation logic from the view logic, and 
eventually reuse it in other parts of the app, the coordinator pattern 
can be adopted in conjunction with the navigator pattern, in which:

* The **Coordinator** decides where to go - ex: if the user is logged in
go to the account screen, otherwise go to the Login screen.
* The **Navigator** decides how to go - ex: by starting an intent to 
open the account screen.

```
                       +---------+
                       |Navigator|
                       +----+----+
                            ^
                            | asc
                            |
+---------+           +-----------+
|   View  |---------->|Coordinator|
+---------+    asc    +-----------+

Legend:
asc = association
```


## Alternatives

The proposed solution is not ready to take advantage of Google Play's 
Dynamic Feature Module (DFM) functionality because feature modules need 
to depend on `app` and not like the reverse. 

```
:app
+-------------------------+
|                         |
+---+-----------------+---+
    ^                 ^
    |                 | 
+-------+         +-------+
|       |         |       |
+-------+         +-------+
:featureX         :featureY
```

Even though it seems a simple change at a high level, at a module level 
it could require a significant amount of work and time in order to 
comply with all the necessary changes. Not only because some libraries 
may need to be initialized or configured at a project level (ex: RealmDb)
but also due to the integration and configuration required by and with 
Google Play. 

Currently, the author of this RFC doesn't have enough experience with 
DFM and their limitations in order to propose a solid and viable 
alternative but is sure that even though the proposed architecture is 
not sufficent to support them, it definitely makes it easier to achieve
that goal in the future than a typical module-by-layer or monolithic 
architecture.

Finally, it's important to emphasize that DFM are only supported by 
Google Play, so users that use other application stores wouldn't be able
to take advantage of this feature and as such, a change of this 
magnitude would have to be considered carefully wether it would be worth
the change or not.

## Other
One could argue that because this is a "green field project", it would 
make sense to start it with DFM but the author suspects that the 
possibility of monzo having DFM integrated is rather low, since this is 
a relatively recent feature. 

Moreover adopting such technology would require a structural change to 
the app and a considerable amount of time for the possible value that 
could be taken from it in a company that is constantly moving and 
growing at a rapid pace.

The graphs were created with [ASCIIFlow](http://asciiflow.com).