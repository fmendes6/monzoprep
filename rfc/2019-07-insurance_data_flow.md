Date: 27/07/2019
* Author: Filipe Mendes

## Summary
Defining the data flow for the insurance feature

## Motivation
Understanding the data flow between the two feature modules (dashboard
and insurance) and their android libraries in order to implement the 
medical insurance feature.

## Proposal

The travel feature module is responsible to show the user the option
to choose an insurance, display a screen with its full details
and let the user subscribe/cancel it.

This can be achievable by interacting directly with an insurance
repository to display the necessary data and perform the operations 
described.

```
:feature_insurance    :insurance_repo
+-------------+       +-------------+
|             |------>|             |
|             |       |             |              
+-------------+       +----------+--+              
                                 |                 
                                 |                 
                      :domain    v                 
                      +----------+--+              
                      |             |              
                      |             |              
                      +----------+--+              
                                 ^                 
                                 |                 
:feature_dashboard    :usecases  |         :domain_repo
+-------------+       +----------+--+      +-------------+
|             |------>+             |----->|             |
|             |       |             |      |             |
+-------------+       +-------------+      +-------------+

```
However, the dashboard needs to adapt its account screen
depending on whether the user has a subscription assigned or not. 

The dashboard feature could use the Insurance Repository 
but its important to keep abstracting dependencies, responsibilities 
and operations in a multi-module architecture.

This means that the dashboard module shouldn't know which operations 
are available in specific feature modules but it should be able to 
answer a "few basic domain questions" in order to adapt its application 
flow.

As such, a new `:usecases` module should be created with simple usecase
classes that can answer various generic questions across different
modules by interacting with a repository that saves the bare minimum
information about specific domains.
 
In the insurance example, the dashboard should be able to access the ID 
of the current enabled insurance but not the full insurance object. 
If the user story requires an action over that insurance, a screen from
the `:feature_insurance` module should be opened with that ID and a
full insurance object can be accessed normally.

```
:usecase

fun UserHasMedicalInsuranceAssignedUseCase() : Boolean {
 return userServicesRepo.containsService(MEDICAL_INSURANCE)
}

:domain
Enum UserAssignedServices {    
   MEDICAL_INSURANCE,    
   SAVINGS 
}
```

## Alternatives

Having a direct dependency on a Insurance Repository could simplify the 
solution but it would enable the dashboard module to perform operations 
on the insurance repositories. Moreover, it would require the dashboard 
to depend on the insurance repository and all its (eventually heavy)
implementations.