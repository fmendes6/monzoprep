package com.monzoprep.feature_travel

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.appbar.AppBarLayout
import kotlinx.android.synthetic.main.activity_travel.*
import kotlin.math.abs

class TravelActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_travel)
        setSupportActionBar(travel_toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        setAlphaAnimation()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun setAlphaAnimation() {
        travel_appbar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
            val alpha = alphaUpToThreeQuartersOfViewHeight(appBarLayout, verticalOffset)
            travel_description_toolbar.alpha = alpha
        })
    }

    private fun alphaUpToThreeQuartersOfViewHeight(appBarLayout: AppBarLayout, verticalOffset: Int): Float {
        val threeQuarters = 0.75f
        val currentOffset = abs(verticalOffset / appBarLayout.totalScrollRange.toFloat())

        return threeQuarters - currentOffset
    }
}