package com.monzoprep.library_insurance_repository_room

import com.monzoprep.library_insurance_repository_room.mapper.InsuranceMapper
import org.junit.Assert.assertEquals
import org.junit.Test
import testdata.InsuranceRoomTestDataBuilder.Companion.anInsuranceRoom

class InsuranceMapTest {

    private val insuranceMapper = InsuranceMapper()

    @Test
    fun `should map from room to domain model`() {
        // Arrange
        val insuranceRoom = anInsuranceRoom()

        // Act
        val insurance = insuranceMapper.map(insuranceRoom)

        // Assert
        assertEquals(insurance.uuid, insuranceRoom.uuid)
        assertEquals(insurance.providedBy, insuranceRoom.providedBy)
        assertEquals(insurance.insuranceType.ordinal, insuranceRoom.insuranceTypeOrdinal)
        assertEquals(insurance.region.ordinal, insuranceRoom.regionOrdinal)
        assertEquals(insurance.cost, insuranceRoom.cost)
        assertEquals(insurance.benefit1, insuranceRoom.benefit1)
        assertEquals(insurance.benefit2, insuranceRoom.benefit2)
        assertEquals(insurance.benefit3, insuranceRoom.benefit3)
    }

    @Test
    fun `should map list from room to domain model`() {
        // Arrange
        val insuranceRoom = anInsuranceRoom()
        val insuranceRoom1 = anInsuranceRoom()
        val insuranceRoom2 = anInsuranceRoom()
        val insurancesList = listOf(insuranceRoom, insuranceRoom1, insuranceRoom2)

        // Act
        val mappedInsurances = insuranceMapper.map(insurancesList)

        // Assert
        assertEquals(mappedInsurances.size, insurancesList.size)
    }
}