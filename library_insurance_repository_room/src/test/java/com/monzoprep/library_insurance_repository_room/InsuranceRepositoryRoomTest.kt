package com.monzoprep.library_insurance_repository_room

import com.monzoprep.library_insurance_repository_room.dao.InsurancesDao
import com.monzoprep.library_insurance_repository_room.mapper.InsuranceMapper
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.Flowable
import io.reactivex.Maybe
import org.junit.Test
import testdata.InsuranceRoomTestDataBuilder.Companion.anInsuranceRoom

class InsuranceRepositoryRoomTest {

    private val insurancesDao = mock<InsurancesDao>()
    private val insuranceMapper = InsuranceMapper()

    private val repository = InsuranceRepositoryRoom(
        insurancesDao, insuranceMapper
    )

    @Test
    fun `should return mapped insurances from dao`() {
        // Arrange
        val insurance1 = anInsuranceRoom()
        val insurance2 = anInsuranceRoom(uuid = "uuid2")
        val listInsurances = listOf(insurance1, insurance2)
        given(insurancesDao.getAllInsurances()).willReturn(Flowable.just(listInsurances))

        // Act
        val insurances = repository.getAllInsurances().test()

        // Assert
        insurances.assertComplete()
        insurances.assertValue {
            val assertSize = it.size == listInsurances.size
            return@assertValue assertSize
        }
    }

    @Test
    fun `should return zero insurances from repository if dao empty`() {
        // Arrange
        given(insurancesDao.getAllInsurances()).willReturn(Flowable.just(listOf()))

        // Act
        val insurances = repository.getAllInsurances().test()

        // Assert
        insurances.assertComplete()
        insurances.assertValue {
            val assertSize = it.size == 0
            return@assertValue assertSize
        }
    }

    @Test
    fun `should return mapped insurance from dao`() {
        // Arrange
        val insuranceRoom = anInsuranceRoom(uuid = "uuid2")
        given(insurancesDao.getInsuranceById(any())).willReturn(Maybe.just(insuranceRoom))

        // Act
        val insurances = repository.getInsuranceById(insuranceRoom.uuid).test()

        // Assert
        insurances.assertComplete()
        insurances.assertValue {
            val assertUUid = it.uuid == insuranceRoom.uuid
            val assertProvidedBy = it.providedBy == insuranceRoom.providedBy
            return@assertValue assertUUid && assertProvidedBy
        }
    }

    @Test
    fun `should return no values if no values in database`() {
        // Arrange
        val insuranceRoom = anInsuranceRoom(uuid = "uuid")
        given(insurancesDao.getInsuranceById(any())).willReturn(Maybe.empty())

        // Act
        val insurances = repository.getInsuranceById(insuranceRoom.uuid).test()

        // Assert
        insurances.assertComplete()
        insurances.assertNoValues()
    }
}