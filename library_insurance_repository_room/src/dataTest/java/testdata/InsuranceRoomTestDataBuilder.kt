package testdata

import com.monzoprep.library_insurance_repository_room.model.InsuranceRoom
import java.math.BigDecimal

class InsuranceRoomTestDataBuilder {
    companion object {
        fun anInsuranceRoom(
            uuid: String = "uuid",
            providedBy: String = "monzo",
            insuranceTypeOrdinal: Int = 0,
            cost: BigDecimal = BigDecimal("1.25"),
            benefit1: String = "benefit1",
            benefit2: String = "benefit2",
            benefit3: String = "benefit3",
            regionOrdinal: Int = 0
        ) = InsuranceRoom(
            uuid,
            providedBy,
            insuranceTypeOrdinal,
            cost,
            benefit1,
            benefit2,
            benefit3,
            regionOrdinal
        )
    }
}