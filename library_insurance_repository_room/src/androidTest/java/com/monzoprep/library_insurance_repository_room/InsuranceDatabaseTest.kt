package com.monzoprep.library_insurance_repository_room

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.monzoprep.library_insurance_repository_room.dao.InsurancesDao
import com.monzoprep.library_insurance_repository_room.database.InsuranceDatabase
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import testdata.InsuranceRoomTestDataBuilder.Companion.anInsuranceRoom

@RunWith(AndroidJUnit4::class)
class InsuranceDatabaseTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var insurancesDao: InsurancesDao
    private lateinit var db: InsuranceDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()

        db = Room.inMemoryDatabaseBuilder(
            context,
            InsuranceDatabase::class.java
        ).build()

        insurancesDao = db.insurancesDao()
    }

    @Test
    fun shouldLoadAllInsurances() {
        // Arrange
        val insurance1 = anInsuranceRoom()
        val insurance2 = anInsuranceRoom(uuid = "uuid2")
        val listInsurances = listOf(insurance1, insurance2)

        // Act
        insurancesDao.insert(listInsurances)
        val insurances = insurancesDao.getAllInsurances().take(1)

        // Assert
        val test = insurances.test()
        test.assertComplete()
        test.assertValue {
            val assertListSize = it.size == listInsurances.size
            val assertInsurance1Exists = it.contains(insurance1)
            val assertInsurance2Exists = it.contains(insurance2)

            return@assertValue assertListSize && assertInsurance1Exists && assertInsurance2Exists
        }
    }

    @Test
    fun shouldLoadCorrectInsurance() {
        // Arrange
        val insurance1 = anInsuranceRoom()
        val insurance2 = anInsuranceRoom(uuid = "uuid2")
        val insurance3 = anInsuranceRoom(uuid = "uuid3")
        val listInsurances = listOf(insurance1, insurance2, insurance3)

        // Act
        insurancesDao.insert(listInsurances)
        val insurance = insurancesDao.getInsuranceById(insurance2.uuid)

        // Assert
        val test = insurance.test()
        test.assertComplete()
        test.assertValue {
            val assertIsInsurance2 = it.uuid == insurance2.uuid
            val assertIsNotInsurance1 = it.uuid != insurance1.uuid
            val assertIsNotInsurance3 = it.uuid != insurance3.uuid

            return@assertValue assertIsNotInsurance1 && assertIsInsurance2 && assertIsNotInsurance3
        }
    }

    @Test
    fun shouldReturnNoValuesIfDatabaseEmpty() {
        // Arrange
        val insurance1 = anInsuranceRoom()

        // Act
        val insurance = insurancesDao.getInsuranceById(insurance1.uuid)

        // Assert
        val test = insurance.test()
        test.assertComplete()
        test.assertNoValues()
    }

    @After
    fun closeDb() {
        db.close()
    }
}