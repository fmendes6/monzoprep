package com.monzoprep.library_insurance_repository_room.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.math.BigDecimal

@Entity
data class InsuranceRoom(
    @PrimaryKey
    @ColumnInfo(name = "uuid") val uuid: String,
    @ColumnInfo(name = "providedBy") val providedBy: String,
    @ColumnInfo(name = "insuranceType") val insuranceTypeOrdinal: Int,
    @ColumnInfo(name = "cost") val cost: BigDecimal,
    @ColumnInfo(name = "benefit1") val benefit1: String,
    @ColumnInfo(name = "benefit2") val benefit2: String,
    @ColumnInfo(name = "benefit3") val benefit3: String,
    @ColumnInfo(name = "region") val regionOrdinal: Int
)