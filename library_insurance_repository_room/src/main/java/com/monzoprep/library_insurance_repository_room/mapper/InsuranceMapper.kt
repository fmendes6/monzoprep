package com.monzoprep.library_insurance_repository_room.mapper

import com.monzoprep.domain.models.insurance.Insurance
import com.monzoprep.domain.models.insurance.InsuranceRegion
import com.monzoprep.domain.models.insurance.InsuranceType
import com.monzoprep.library_insurance_repository_room.model.InsuranceRoom

class InsuranceMapper {

    fun map(insuranceRoom: InsuranceRoom): Insurance {
        return Insurance(
            insuranceRoom.uuid,
            insuranceRoom.providedBy,
            InsuranceType.values()[insuranceRoom.insuranceTypeOrdinal],
            insuranceRoom.cost,
            insuranceRoom.benefit1,
            insuranceRoom.benefit2,
            insuranceRoom.benefit3,
            InsuranceRegion.values()[insuranceRoom.regionOrdinal]
        )
    }

    fun map(insuranceRooms: List<InsuranceRoom>) = insuranceRooms.map { map(it) }
}