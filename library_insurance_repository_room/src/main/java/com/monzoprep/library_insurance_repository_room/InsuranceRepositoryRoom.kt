package com.monzoprep.library_insurance_repository_room

import com.monzoprep.domain.models.insurance.Insurance
import com.monzoprep.library_insurance_repository.InsuranceRepository
import com.monzoprep.library_insurance_repository_room.dao.InsurancesDao
import com.monzoprep.library_insurance_repository_room.mapper.InsuranceMapper
import io.reactivex.Flowable
import io.reactivex.Maybe

class InsuranceRepositoryRoom(
    private val insurancesDao: InsurancesDao,
    private val insuranceMapper: InsuranceMapper
) : InsuranceRepository {
    override fun getAllInsurances(): Flowable<List<Insurance>> {
        return insurancesDao
            .getAllInsurances()
            .map { insuranceMapper.map(it) }
    }

    override fun getInsuranceById(uuid: String): Maybe<Insurance> {
        return insurancesDao
            .getInsuranceById(uuid)
            .map { insuranceMapper.map(it) }
    }
}