package com.monzoprep.library_insurance_repository_room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.monzoprep.library_insurance_repository_room.model.InsuranceRoom
import io.reactivex.Flowable
import io.reactivex.Maybe

@Dao
interface InsurancesDao {

    @Query("SELECT * FROM InsuranceRoom")
    fun getAllInsurances(): Flowable<List<InsuranceRoom>>

    @Query("SELECT * FROM InsuranceRoom WHERE uuid = (:insuranceUuid)")
    fun getInsuranceById(insuranceUuid: String): Maybe<InsuranceRoom>

    @Insert
    fun insert(insurances: List<InsuranceRoom>)
}
