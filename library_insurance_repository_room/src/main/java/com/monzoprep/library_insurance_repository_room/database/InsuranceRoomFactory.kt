package com.monzoprep.library_insurance_repository_room.database

import com.monzoprep.domain.models.insurance.InsuranceRegion
import com.monzoprep.domain.models.insurance.InsuranceType
import com.monzoprep.library_insurance_repository_room.model.InsuranceRoom
import java.math.BigDecimal
import java.util.*
import kotlin.random.Random

class InsuranceRoomFactory {

    companion object {
        fun createInsurances(number: Int): List<InsuranceRoom> {
            val list = mutableListOf<InsuranceRoom>()
            repeat(number) {
                list.add(createInsurance())
            }
            return list
        }

        private fun createInsurance(): InsuranceRoom {
            return InsuranceRoom(
                UUID.randomUUID().toString(),
                random(listOf("AXA", "XPTO", "MONZO", "XYZ", "ABC")),
                InsuranceType.PayPerDay.ordinal,
                random(listOf(BigDecimal("0.99"), BigDecimal("1.25"), BigDecimal("1.10"))),
                "Emergency Medical Assistance",
                "Emergency Dental Treatment",
                "Next day Refund",
                random(
                    listOf(
                        InsuranceRegion.Europe.ordinal,
                        InsuranceRegion.NorthAmerica.ordinal,
                        InsuranceRegion.RestOfWorld.ordinal
                    )
                )
            )
        }

        private fun <T> random(options: List<T>): T {
            return options[Random.nextInt(options.size)]
        }
    }
}