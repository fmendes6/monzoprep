package com.monzoprep.library_insurance_repository_room.database

import android.content.Context

class InsuranceDatabaseWrapper(val context: Context) {
    fun getInsurancesDao() = InsuranceDatabase.getInstance(context).insurancesDao()
}