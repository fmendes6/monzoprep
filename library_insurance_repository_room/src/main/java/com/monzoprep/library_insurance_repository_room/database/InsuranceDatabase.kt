package com.monzoprep.library_insurance_repository_room.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.monzoprep.library_insurance_repository_room.dao.InsurancesDao
import com.monzoprep.library_insurance_repository_room.model.InsuranceRoom
import java.util.concurrent.Executors

@Database(entities = [InsuranceRoom::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
internal abstract class InsuranceDatabase : RoomDatabase() {
    abstract fun insurancesDao(): InsurancesDao

    companion object {

        @Volatile
        private var INSTANCE: InsuranceDatabase? = null
        private val DATABASENAME = "database-insurances"

        fun getInstance(context: Context): InsuranceDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                InsuranceDatabase::class.java,
                DATABASENAME
            ).addCallback(object : Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
                    insertDataOnDatabaseCreation(context)
                }
            }).build()

        private fun insertDataOnDatabaseCreation(context: Context) {
            Executors.newSingleThreadExecutor().execute {
                getInstance(context)
                    .insurancesDao()
                    .insert(baseData)
            }
        }

        private val baseData = InsuranceRoomFactory.createInsurances(10)
    }
}