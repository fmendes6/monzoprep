package com.monzoprep.feature_dashboard.view.fragments

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.monzoprep.core.test.CoroutinesTestRule
import com.monzoprep.domain.UserServiceTestDataBuilder.Companion.aUserService
import com.monzoprep.domain.models.UserServiceType
import com.monzoprep.feature_dashboard.navigation.DashboardCoordinator
import com.monzoprep.feature_dashboard.view.fragments.utils.observeForTesting
import com.monzoprep.library_domain_usecases.GetUserAssignedService
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test

class AccountViewModelTest {
    private val coordinator = mock<DashboardCoordinator>()
    private val usecase = mock<GetUserAssignedService>()

    private val viewModel = AccountViewModel(usecase, coordinator)

    @ExperimentalCoroutinesApi
    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @Test
    fun `should set viewstate with repository value`() {
        coroutinesTestRule.testDispatcher.runBlockingTest {
            // Arrange
            val userServiceType = UserServiceType.MEDICAL_INSURANCE
            val userService = aUserService(userServiceType = userServiceType, serviceUuid = "newUUID")
            given(usecase.get(userServiceType)).willReturn(userService)

            // Act
            viewModel.verifyInsuranceStatus()

            // Assert
            verify(usecase).get(userServiceType)
            viewModel.viewState.observeForTesting {
                assertEquals(viewModel.viewState.value!!.insuranceUuid, userService.serviceUuid)
                assertEquals(viewModel.viewState.value!!.userServiceType, userService.userServiceType)
            }
        }
    }

    /*
     * https://github.com/Kotlin/kotlinx.coroutines/issues/1205
     * "...runBlockingTest uses async to run the test body
     * by the time it tries to throw an exception, the test has already completed."

    @ExperimentalCoroutinesApi
    @Test
    @Throws(NullPointerException::class)
    fun `should throw exception if no userservice available`() {
        coroutinesTestRule.testDispatcher.runBlockingTest {
            // Arrange
            val userServiceType = UserServiceType.MEDICAL_INSURANCE
            given(usecase.get(userServiceType)).willReturn(null)

            // Act
            viewModel.verifyInsuranceStatus()
        }
    }
    */

    @Test
    fun `should invoke coordinator when insurance button clicked`() {
        // Arrange
        val accountViewState = AccountViewState()
        viewModel.viewState.value = accountViewState

        // Act
        viewModel.insuranceOptionClicked()

        // Assert
        verify(coordinator).medicalInsuranceClicked(any(), any())
    }

    @Test
    fun `should not invoke coordinator when no insurance and insurance button clicked`() {
        // Act
        viewModel.insuranceOptionClicked()

        // Assert
        verify(coordinator, never()).medicalInsuranceClicked(any(), any())
    }
}