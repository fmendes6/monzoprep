package com.monzoprep.feature_dashboard.view.fragments

import com.monzoprep.feature_dashboard.navigation.DashboardCoordinator
import com.monzoprep.feature_dashboard.navigation.DashboardNavigator
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import org.junit.Test

class DashboardCoordinatorTest {

    private val dashboardNavigator = mock<DashboardNavigator>()
    private val dashboardCoordinator = DashboardCoordinator(dashboardNavigator)

    @Test
    fun `should show template when home button clicked`() {
        // Act
        dashboardCoordinator.homeButtonClicked()

        // Assert
        verify(dashboardNavigator).showTemplate()
    }

    @Test
    fun `should show template when payments button clicked`() {
        // Act
        dashboardCoordinator.paymentsButtonClicked()

        // Assert
        verify(dashboardNavigator).showTemplate()
    }

    @Test
    fun `should show template when summary button clicked`() {
        // Act
        dashboardCoordinator.summaryButtonClicked()

        // Assert
        verify(dashboardNavigator).showTemplate()
    }

    @Test
    fun `should show help when help button clicked`() {
        // Act
        dashboardCoordinator.helpButtonClicked()

        // Assert
        verify(dashboardNavigator).showHelp()
    }

    @Test
    fun `should show account when account button clicked`() {
        // Act
        dashboardCoordinator.accountButtonClicked()

        // Assert
        verify(dashboardNavigator).showAccount()
    }

    @Test
    fun `should show travel when travel view clicked`() {
        // Act
        dashboardCoordinator.travelOptionClicked()

        // Assert
        verify(dashboardNavigator).showTravelWithMonzo()
    }

    @Test
    fun `should show insurance details when insurance button clicked`() {
        // Arrange
        val hasInsurance = true
        val insuranceId = "uuid"

        // Act
        dashboardCoordinator.medicalInsuranceClicked(hasInsurance, insuranceId)

        // Assert
        verify(dashboardNavigator).showInsuranceDetail(hasInsurance, insuranceId)
    }

    @Test
    fun `should show insurance chooser when no insurance and insurance button clicked`() {
        // Arrange
        val hasInsurance = false
        val insuranceId = ""

        // Act
        dashboardCoordinator.medicalInsuranceClicked(hasInsurance, insuranceId)

        // Assert
        verify(dashboardNavigator).showInsuranceChooser()
    }

    @Test
    fun `should not recreate home fragment when already visible`() {
        // Act
        dashboardCoordinator.homeButtonClicked()
        dashboardCoordinator.homeButtonClicked()

        // Assert
        verify(dashboardNavigator, times(1)).showTemplate()
    }

    @Test
    fun `should not recreate summary fragment when already visible`() {
        // Act
        dashboardCoordinator.summaryButtonClicked()
        dashboardCoordinator.summaryButtonClicked()

        // Assert
        verify(dashboardNavigator, times(1)).showTemplate()
    }

    @Test
    fun `should not recreate help fragment when already visible`() {
        // Act
        dashboardCoordinator.helpButtonClicked()
        dashboardCoordinator.helpButtonClicked()

        // Assert
        verify(dashboardNavigator, times(1)).showHelp()
    }

    @Test
    fun `should not recreate payments fragment when already visible`() {
        // Act
        dashboardCoordinator.paymentsButtonClicked()
        dashboardCoordinator.paymentsButtonClicked()

        // Assert
        verify(dashboardNavigator, times(1)).showTemplate()
    }

    @Test
    fun `should not recreate account fragment when already visible`() {
        // Act
        dashboardCoordinator.accountButtonClicked()
        dashboardCoordinator.accountButtonClicked()

        // Assert
        verify(dashboardNavigator, times(1)).showAccount()
    }
}