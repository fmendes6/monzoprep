package com.monzoprep.feature_dashboard.view.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.monzoprep.feature_dashboard.R
import com.monzoprep.feature_dashboard.view.DashboardActivity
import kotlinx.android.synthetic.main.fragment_help.*

class HelpFragment : BaseFragment() {

    private val fragmentTitle by lazy { getString(R.string.menu_help) }
    private lateinit var viewModel: HelpViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_help, container, false)
        viewModel = ViewModelProviders.of(activity!!, viewModelFactory)[HelpViewModel::class.java]
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        travel_card.setOnClickListener {
            viewModel.travelWithMonzoViewClicked()
        }
    }

    override fun onResume() {
        super.onResume()
        (activity as DashboardActivity).setToolbarTitle(fragmentTitle)
    }

    private fun openActivity(activity: Activity, action: String) {
        val intent = Intent(action)
        intent.setPackage(activity.packageName)
        activity.startActivity(intent)
    }
}
