package com.monzoprep.feature_dashboard.view.fragments

import com.monzoprep.domain.models.UserServiceType

internal data class AccountViewState(
    val userServiceType: UserServiceType = UserServiceType.MEDICAL_INSURANCE,
    val insuranceUuid: String = ""
) {
    fun hasInsurance() = insuranceUuid.isNotEmpty()
}