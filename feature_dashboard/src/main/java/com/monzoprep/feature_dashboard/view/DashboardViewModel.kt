package com.monzoprep.feature_dashboard.view

import androidx.lifecycle.ViewModel
import com.monzoprep.feature_dashboard.navigation.DashboardCoordinator
import javax.inject.Inject

internal class DashboardViewModel @Inject constructor(
    private val dashboardCoordinator: DashboardCoordinator
) : ViewModel() {

    fun showHome() = dashboardCoordinator.homeButtonClicked()
    fun showSummary() = dashboardCoordinator.summaryButtonClicked()
    fun showAccount() = dashboardCoordinator.accountButtonClicked()
    fun showPayments() = dashboardCoordinator.paymentsButtonClicked()
    fun showHelp() = dashboardCoordinator.helpButtonClicked()
}