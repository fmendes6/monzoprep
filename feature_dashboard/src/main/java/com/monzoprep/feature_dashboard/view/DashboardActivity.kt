package com.monzoprep.feature_dashboard.view

import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.monzoprep.feature_dashboard.R
import kotlinx.android.synthetic.main.activity_dashboard.*

internal class DashboardActivity : BaseActivity() {

    private lateinit var viewModel: DashboardViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        setSupportActionBar(dashboardToolbar)
        viewModel = ViewModelProviders.of(this, viewModelFactory)[DashboardViewModel::class.java]
        viewModel.showHome()
        setNavigationListener()
    }

    private fun setNavigationListener() {
        bottom_navigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.action_home -> viewModel.showHome()
                R.id.action_summary -> viewModel.showSummary()
                R.id.action_account -> viewModel.showAccount()
                R.id.action_payments -> viewModel.showPayments()
                R.id.action_help -> viewModel.showHelp()
                else -> viewModel.showHome()
            }
        }
    }

    fun setToolbarTitle(title: String) {
        dashboardToolbar.title = title
    }
}
