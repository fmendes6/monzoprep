package com.monzoprep.feature_dashboard.view.fragments

import androidx.lifecycle.ViewModel
import com.monzoprep.feature_dashboard.navigation.DashboardCoordinator
import javax.inject.Inject

internal class HelpViewModel @Inject constructor(
    private val coordinator: DashboardCoordinator
) : ViewModel() {

    internal fun travelWithMonzoViewClicked() {
        coordinator.travelOptionClicked()
    }
}
