package com.monzoprep.feature_dashboard.view.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.monzoprep.core.di.CoreInjectHelper
import com.monzoprep.feature_dashboard.di.DaggerDashboardComponent
import com.monzoprep.feature_dashboard.navigation.DashboardNavigator
import javax.inject.Inject

open class BaseFragment : Fragment() {

    @Inject
    internal lateinit var dashboardNavigator: DashboardNavigator

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startDi()
        dashboardNavigator.activity = activity
    }

    override fun onDestroy() {
        super.onDestroy()
        dashboardNavigator.activity = activity
    }

    private fun startDi() {
        DaggerDashboardComponent
            .builder()
            .coreComponent(CoreInjectHelper.provideCoreComponent(activity!!.applicationContext))
            .build()
            .inject(this)
    }
}