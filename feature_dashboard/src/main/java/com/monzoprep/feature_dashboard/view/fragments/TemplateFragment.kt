package com.monzoprep.feature_dashboard.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.monzoprep.feature_dashboard.R
import com.monzoprep.feature_dashboard.view.DashboardActivity

class TemplateFragment : Fragment() {

    private val fragmentTitle by lazy { getString(R.string.menu_empty) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_template, container, false)
    }

    override fun onResume() {
        super.onResume()
        (activity as DashboardActivity).setToolbarTitle(fragmentTitle)
    }
}
