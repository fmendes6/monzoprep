package com.monzoprep.feature_dashboard.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.monzoprep.feature_dashboard.R
import com.monzoprep.feature_dashboard.view.DashboardActivity
import kotlinx.android.synthetic.main.fragment_account.*

class AccountFragment : BaseFragment() {

    private lateinit var viewModel: AccountViewModel
    private val fragmentTitle by lazy { getString(R.string.menu_account) }
    private var insuranceState = AccountViewState()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_account, container, false)
        viewModel = ViewModelProviders.of(activity!!, viewModelFactory)[AccountViewModel::class.java]
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.viewState.observe(this, Observer { state ->
            insuranceState = state
            val resource = getStringForInsuranceLabel(insuranceState.hasInsurance())
            account_insurance_description_label.text = resource
        })

        viewModel.verifyInsuranceStatus()

        insurance_clickable_view.setOnClickListener {
            viewModel.insuranceOptionClicked()
        }
    }

    private fun getStringForInsuranceLabel(hasInsurance: Boolean): String {
        val resourceId = if (hasInsurance) {
            R.string.account_insurance_description_label
        } else {
            R.string.account_insurance_description_add_new_label
        }
        return getString(resourceId)
    }

    override fun onResume() {
        super.onResume()
        (activity as DashboardActivity).setToolbarTitle(fragmentTitle)
    }
}
