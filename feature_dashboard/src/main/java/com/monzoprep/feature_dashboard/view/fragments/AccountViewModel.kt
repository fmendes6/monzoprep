package com.monzoprep.feature_dashboard.view.fragments

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.monzoprep.domain.models.UserServiceType
import com.monzoprep.feature_dashboard.navigation.DashboardCoordinator
import com.monzoprep.library_domain_usecases.GetUserAssignedService
import kotlinx.coroutines.launch
import javax.inject.Inject

internal class AccountViewModel @Inject constructor(
    private val getUserHasMedicalInsurance: GetUserAssignedService,
    private val coordinator: DashboardCoordinator
) : ViewModel() {

    internal val viewState: MutableLiveData<AccountViewState> by lazy {
        MutableLiveData<AccountViewState>()
    }

    internal fun verifyInsuranceStatus() {
        viewModelScope.launch {
            val insurance = getUserHasMedicalInsurance.get(UserServiceType.MEDICAL_INSURANCE)
            val accountViewState = AccountViewState(insurance.userServiceType, insurance.serviceUuid)
            viewState.value = accountViewState
        }
    }

    internal fun insuranceOptionClicked() {
        viewState.value?.let {
            coordinator.medicalInsuranceClicked(it.hasInsurance(), it.insuranceUuid)
        }
    }
}
