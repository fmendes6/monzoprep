package com.monzoprep.feature_dashboard.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.monzoprep.core.di.CoreInjectHelper
import com.monzoprep.feature_dashboard.di.DaggerDashboardComponent
import com.monzoprep.feature_dashboard.navigation.DashboardNavigator
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity() {

    @Inject
    internal lateinit var dashboardNavigator: DashboardNavigator

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startDi()
        dashboardNavigator.activity = this
    }

    override fun onDestroy() {
        super.onDestroy()
        dashboardNavigator.activity = null
    }

    private fun startDi() {
        DaggerDashboardComponent
            .builder()
            .coreComponent(CoreInjectHelper.provideCoreComponent(applicationContext))
            .build()
            .inject(this)
    }
}