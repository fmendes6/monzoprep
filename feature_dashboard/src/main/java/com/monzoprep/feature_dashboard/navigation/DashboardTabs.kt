package com.monzoprep.feature_dashboard.navigation

enum class DashboardTabs {
    Home, Summary, Account, Payments, Help, None
}