package com.monzoprep.feature_dashboard.navigation

internal class DashboardCoordinator(private val dashboardNavigator: DashboardNavigator) {

    private var currentTab = DashboardTabs.None

    internal fun homeButtonClicked(): Boolean {
        return selectTab(DashboardTabs.Home) {
            dashboardNavigator.showTemplate()
        }
    }

    internal fun summaryButtonClicked(): Boolean {
        return selectTab(DashboardTabs.Summary) {
            dashboardNavigator.showTemplate()
        }
    }

    internal fun accountButtonClicked(): Boolean {
        return selectTab(DashboardTabs.Account) {
            dashboardNavigator.showAccount()
        }
    }

    internal fun paymentsButtonClicked(): Boolean {
        return selectTab(DashboardTabs.Payments) {
            dashboardNavigator.showTemplate()
        }
    }

    internal fun helpButtonClicked(): Boolean {
        return selectTab(DashboardTabs.Help) {
            dashboardNavigator.showHelp()
        }
    }

    internal fun medicalInsuranceClicked(hasInsurance: Boolean, insuranceUuid: String) {
        if (hasInsurance) {
            dashboardNavigator.showInsuranceDetail(true, insuranceUuid)
        } else {
            dashboardNavigator.showInsuranceChooser()
        }
    }

    internal fun travelOptionClicked() {
        dashboardNavigator.showTravelWithMonzo()
    }

    private fun selectTab(tab: DashboardTabs, showFragment: (() -> Unit)): Boolean {
        if (currentTab != tab) {
            showFragment()
            currentTab = tab
            return true
        }
        return false
    }
}