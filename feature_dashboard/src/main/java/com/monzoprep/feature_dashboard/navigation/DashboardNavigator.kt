package com.monzoprep.feature_dashboard.navigation

import android.app.Activity
import android.content.Intent
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.monzoprep.core.navigation.NavigationActions
import com.monzoprep.core.view.IntentKeys
import com.monzoprep.feature_dashboard.R
import com.monzoprep.feature_dashboard.view.fragments.AccountFragment
import com.monzoprep.feature_dashboard.view.fragments.HelpFragment
import com.monzoprep.feature_dashboard.view.fragments.TemplateFragment

internal class DashboardNavigator {

    var activity: Activity? = null

    fun showTemplate() = showFragment(activity as FragmentActivity, TemplateFragment())

    fun showAccount() = showFragment(activity as FragmentActivity, AccountFragment())

    fun showHelp() = showFragment(activity as FragmentActivity, HelpFragment())

    fun showTravelWithMonzo() = activity?.let {
        openActivity(it, NavigationActions.Travel)
    }

    fun showInsuranceDetail(hasInsuranceEnabled: Boolean, insuranceUuid: String) = activity?.let {
        val intent = Intent(NavigationActions.InsuranceDetail)
        intent.setPackage(it.packageName)
        intent.putExtra(IntentKeys.InsuranceDetailInsuraceId, insuranceUuid)
        intent.putExtra(IntentKeys.InsuranceDetailIsInsuraceEnabled, hasInsuranceEnabled)
        it.startActivity(intent)
    }

    fun showInsuranceChooser() = activity?.let {
        openActivity(it, NavigationActions.InsuranceChooser)
    }

    private fun showFragment(fragmentActivity: FragmentActivity, fragment: Fragment) {
        val fragmentManager = fragmentActivity.supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, fragment)
        transaction.commit()
    }

    private fun openActivity(activity: Activity, action: String) {
        val intent = Intent(action)
        intent.setPackage(activity.packageName)
        activity.startActivity(intent)
    }
}