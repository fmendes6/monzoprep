package com.monzoprep.feature_dashboard.di

import com.monzoprep.core.di.FeatureScope
import com.monzoprep.core.di.ViewModelFactoryModule
import com.monzoprep.feature_dashboard.navigation.DashboardCoordinator
import com.monzoprep.feature_dashboard.navigation.DashboardNavigator
import dagger.Module
import dagger.Provides

@Module(
    includes = [
        ViewModelFactoryModule::class,
        DashboardViewModelsModule::class,
        UseCasesModule::class,
        RepositoryModule::class
    ]
)
internal object DashboardModule {

    @JvmStatic
    @Provides
    @FeatureScope
    internal fun provideNavigator(): DashboardNavigator {
        return DashboardNavigator()
    }

    @JvmStatic
    @Provides
    @FeatureScope
    internal fun provideCoordinator(navigator: DashboardNavigator): DashboardCoordinator {
        return DashboardCoordinator(navigator)
    }
}