package com.monzoprep.feature_dashboard.di

import com.monzoprep.core.di.FeatureScope
import com.monzoprep.library_domain_repository.UserAssignedServicesRepository
import com.monzoprep.library_domain_usecases.GetUserAssignedService
import dagger.Module
import dagger.Provides

@Module
internal object UseCasesModule {
    @JvmStatic
    @FeatureScope
    @Provides
    internal fun provideGetUserAssignedService(
        userAssignedServicesRepository: UserAssignedServicesRepository
    ): GetUserAssignedService {
        return GetUserAssignedService(userAssignedServicesRepository)
    }
}