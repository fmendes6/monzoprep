package com.monzoprep.feature_dashboard.di

import androidx.lifecycle.ViewModel
import com.monzoprep.core.di.FeatureScope
import com.monzoprep.core.di.ViewModelKey
import com.monzoprep.feature_dashboard.view.DashboardViewModel
import com.monzoprep.feature_dashboard.view.fragments.AccountViewModel
import com.monzoprep.feature_dashboard.view.fragments.HelpViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class DashboardViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(DashboardViewModel::class)
    @FeatureScope
    internal abstract fun dashboardViewModel(viewModel: DashboardViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AccountViewModel::class)
    @FeatureScope
    internal abstract fun accountViewModel(viewModel: AccountViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HelpViewModel::class)
    @FeatureScope
    internal abstract fun helpViewModel(viewModel: HelpViewModel): ViewModel
}