package com.monzoprep.feature_dashboard.di

import android.content.Context
import com.monzoprep.core.di.FeatureScope
import com.monzoprep.library_domain_repository.UserAssignedServicesRepository
import com.monzoprep.library_domain_repository_room.UserAssignedServicesRepositoryRoom
import com.monzoprep.library_domain_repository_room.dao.UserServicesDao
import com.monzoprep.library_domain_repository_room.database.UserServicesDatabaseWrapper
import com.monzoprep.library_domain_repository_room.mapper.UserServiceMapper
import dagger.Module
import dagger.Provides

@Module
internal object RepositoryModule {

    @JvmStatic
    @FeatureScope
    @Provides
    internal fun provideUserAssignedServicesRepository(
        userServicesDao: UserServicesDao,
        userServiceMapper: UserServiceMapper
    ): UserAssignedServicesRepository {
        return UserAssignedServicesRepositoryRoom(userServicesDao, userServiceMapper)
    }

    @JvmStatic
    @FeatureScope
    @Provides
    internal fun provideUserServicesDao(
        context: Context
    ): UserServicesDao {
        return UserServicesDatabaseWrapper(context).getUserServicesDao()
    }

    @JvmStatic
    @FeatureScope
    @Provides
    internal fun provideUserServiceMapper(): UserServiceMapper {
        return UserServiceMapper()
    }
}