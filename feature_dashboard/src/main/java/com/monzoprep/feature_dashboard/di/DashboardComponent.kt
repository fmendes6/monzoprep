package com.monzoprep.feature_dashboard.di

import com.monzoprep.core.di.CoreComponent
import com.monzoprep.core.di.FeatureScope
import com.monzoprep.feature_dashboard.view.BaseActivity
import com.monzoprep.feature_dashboard.view.fragments.BaseFragment
import dagger.Component

@Component(
    modules = [DashboardModule::class],
    dependencies = [CoreComponent::class]
)
@FeatureScope
interface DashboardComponent {
    fun inject(activity: BaseActivity)
    fun inject(fragment: BaseFragment)
}