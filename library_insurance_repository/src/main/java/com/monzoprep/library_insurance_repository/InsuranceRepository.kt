package com.monzoprep.library_insurance_repository

import com.monzoprep.domain.models.insurance.Insurance
import io.reactivex.Flowable
import io.reactivex.Maybe

interface InsuranceRepository {
    fun getAllInsurances(): Flowable<List<Insurance>>
    fun getInsuranceById(uuid: String): Maybe<Insurance>
}