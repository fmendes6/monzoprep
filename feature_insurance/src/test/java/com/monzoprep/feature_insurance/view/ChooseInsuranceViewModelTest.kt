package com.monzoprep.feature_insurance.view

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.monzoprep.domain.InsuranceTestDataBuilder.Companion.aInsurance
import com.monzoprep.domain.models.insurance.Insurance
import com.monzoprep.domain.models.insurance.InsuranceRegion
import com.monzoprep.feature_insurance.navigation.InsuranceCoordinator
import com.monzoprep.feature_insurance.testdata.InsuranceItemTestDataBuilder.Companion.anInsuranceItem
import com.monzoprep.feature_insurance.utils.RxImmediateSchedulerRule
import com.monzoprep.feature_insurance.utils.observeForTesting
import com.monzoprep.feature_insurance.view.choose_insurance.ChooseInsuranceViewModel
import com.monzoprep.feature_insurance.view.model.InsuranceRow
import com.monzoprep.feature_insurance.view.model.InsuranceRowHeader
import com.monzoprep.library_insurance_repository.InsuranceRepository
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Flowable
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test

class ChooseInsuranceViewModelTest {
    private val insuranceCoordinator = mock<InsuranceCoordinator>()
    private val insuranceViewMapper = mock<InsuranceViewMapper>()
    private val insuranceRepository = mock<InsuranceRepository>()
    private val viewModel = ChooseInsuranceViewModel(insuranceCoordinator, insuranceViewMapper, insuranceRepository)

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val rxImmediateSchedulerRule = RxImmediateSchedulerRule()

    @Test
    fun `should return insurances mapped plus header for same region`() {
        // Arrange
        val listInsurances = createInsurances()
        given(insuranceRepository.getAllInsurances()).willReturn(Flowable.just(listInsurances))

        val insuranceItem1 = anInsuranceItem(uuid = "uuid1", region = "Europe")
        val insuranceItem2 = anInsuranceItem(uuid = "uuid2", region = "Europe")
        val listItems = listOf(insuranceItem1, insuranceItem2)

        given(
            insuranceViewMapper.mapToItems(
                listInsurances,
                viewModel::insuranceSelected
            )
        ).willReturn(listItems)

        // Act
        viewModel.getInsurances()

        // Assert
        viewModel.viewState.observeForTesting {
            val insurancesViewRow = viewModel.viewState.value!!
            assertEquals(insurancesViewRow.size, listItems.size + 1)
            assert(insurancesViewRow[0] is InsuranceRowHeader)
            assert(insurancesViewRow[1] is InsuranceRow)
        }
    }

    @Test
    fun `should return insurances mapped plus headers for different regions`() {
        // Arrange
        val listInsurances = createInsurances()
        given(insuranceRepository.getAllInsurances()).willReturn(Flowable.just(listInsurances))

        val insuranceOptionView1 = anInsuranceItem(uuid = "uuid1", region = "Europe")
        val insuranceOptionView2 = anInsuranceItem(uuid = "uuid2", region = "Europe")
        val insuranceOptionView3 = anInsuranceItem(uuid = "uuid3", region = "America")
        val listItems = listOf(insuranceOptionView1, insuranceOptionView2, insuranceOptionView3)
        given(
            insuranceViewMapper.mapToItems(
                listInsurances,
                viewModel::insuranceSelected
            )
        ).willReturn(listItems)

        // Act
        viewModel.getInsurances()

        // Assert
        viewModel.viewState.observeForTesting {
            val insurancesViewRow = viewModel.viewState.value!!
            assertEquals(insurancesViewRow.size, listItems.size + 2)
            assert(insurancesViewRow[0] is InsuranceRowHeader)
            assert(insurancesViewRow[1] is InsuranceRow)
            assert(insurancesViewRow[2] is InsuranceRow)
            assert(insurancesViewRow[3] is InsuranceRowHeader)
            assert(insurancesViewRow[4] is InsuranceRow)
        }
    }

    @Test
    fun `should show zero insurances when repository throws error`() {
        // Arrange
        given(insuranceRepository.getAllInsurances()).willReturn(Flowable.error(Exception()))

        // Act
        viewModel.getInsurances()

        // Assert
        viewModel.viewState.observeForTesting {
            val insurancesViewRow = viewModel.viewState.value!!
            assertEquals(insurancesViewRow.size, 0)
        }
    }

    @Test
    fun `should show zero insurances when repository returns empty`() {
        // Arrange
        given(insuranceRepository.getAllInsurances()).willReturn(Flowable.empty())

        // Act
        viewModel.getInsurances()

        // Assert
        viewModel.viewState.observeForTesting {
            val insurancesViewRow = viewModel.viewState.value!!
            assertEquals(insurancesViewRow.size, 0)
        }
    }

    @Test
    fun `should invoke coordinator when insurance selected`() {
        // Arrange
        val insuranceId = "uuid"

        // Act
        viewModel.insuranceSelected(insuranceId)

        // Assert
        verify(insuranceCoordinator).insuranceSelected(insuranceId)
    }

    private fun createInsurances(): List<Insurance> {
        val insurance1 = aInsurance(uuid = "uuid1", region = InsuranceRegion.Europe)
        val insurance2 = aInsurance(uuid = "uuid2", region = InsuranceRegion.Europe)
        return listOf(insurance1, insurance2)
    }
}