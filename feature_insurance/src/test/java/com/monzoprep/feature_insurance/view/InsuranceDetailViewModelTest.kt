package com.monzoprep.feature_insurance.view

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.monzoprep.core.test.CoroutinesTestRule
import com.monzoprep.domain.InsuranceTestDataBuilder.Companion.aInsurance
import com.monzoprep.feature_insurance.navigation.InsuranceCoordinator
import com.monzoprep.feature_insurance.testdata.InsuranceViewTestDataBuilder.Companion.aInsuranceView
import com.monzoprep.feature_insurance.utils.RxImmediateSchedulerRule
import com.monzoprep.feature_insurance.utils.observeForTesting
import com.monzoprep.feature_insurance.view.insurance_details.InsuranceDetailViewModel
import com.monzoprep.library_domain_usecases.CancelUserAssignedService
import com.monzoprep.library_domain_usecases.SubscribeUserAssignedService
import com.monzoprep.library_insurance_repository.InsuranceRepository
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Maybe
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Rule
import org.junit.Test

class InsuranceDetailViewModelTest {
    private val insuranceCoordinator = mock<InsuranceCoordinator>()
    private val insuranceViewMapper = mock<InsuranceViewMapper>()
    private val insuranceRepository = mock<InsuranceRepository>()
    private val cancelUserAssignedService = mock<CancelUserAssignedService>()
    private val subscribeUserAssignedService = mock<SubscribeUserAssignedService>()
    private val viewModel = InsuranceDetailViewModel(
        insuranceCoordinator,
        insuranceViewMapper,
        insuranceRepository,
        cancelUserAssignedService,
        subscribeUserAssignedService
    )

    @ExperimentalCoroutinesApi
    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val rxImmediateSchedulerRule = RxImmediateSchedulerRule()

    @Test
    fun `should return insurance mapped to present in view`() {
        // Arrange
        val insurance = aInsurance()
        val insuranceView = aInsuranceView()
        given(insuranceRepository.getInsuranceById(insurance.uuid)).willReturn(Maybe.just(insurance))
        given(insuranceViewMapper.mapToDetails(insurance)).willReturn(insuranceView)

        // Act
        viewModel.getInsurance(insurance.uuid)

        // Assert
        viewModel.viewState.observeForTesting {
            val insuranceViewState = viewModel.viewState.value!!
            assertEquals(insuranceViewState, insuranceView)
        }
    }

    @Test
    fun `should keep viewstate null if insurance not existant`() {
        // Arrange
        val insurance = aInsurance()
        given(insuranceRepository.getInsuranceById(insurance.uuid)).willReturn(Maybe.empty())

        // Act
        viewModel.getInsurance(insurance.uuid)

        // Assert
        viewModel.viewState.observeForTesting {
            assertNull(viewModel.viewState.value)
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `should cancel insurance when confirm button pressed`() {
        // Arrange
        val insurance = aInsurance()
        val hasInsurance = true

        // Act
        coroutinesTestRule.testDispatcher.runBlockingTest {
            viewModel.confirmButtonPressed(hasInsurance, insurance.uuid)

            // Assert
            verify(cancelUserAssignedService).cancel(any())
            verify(insuranceCoordinator).insuranceDetailButtonPressed()
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `should subscribe insurance when confirm button pressed`() {
        // Arrange
        val insurance = aInsurance()
        val hasInsurance = false

        // Act
        coroutinesTestRule.testDispatcher.runBlockingTest {
            viewModel.confirmButtonPressed(hasInsurance, insurance.uuid)

            // Assert
            verify(subscribeUserAssignedService).subscribe(any())
            verify(insuranceCoordinator).insuranceDetailButtonPressed()
        }
    }
}