package com.monzoprep.feature_insurance.testdata

import com.monzoprep.feature_insurance.view.model.InsuranceRow

class InsuranceItemTestDataBuilder {
    companion object {
        fun anInsuranceItem(
            uuid: String = "uuid",
            priceDescription: String = "",
            benefit1: String = "",
            benefit2: String = "",
            providedBy: String = "",
            region: String = ""
        ) = InsuranceRow(
            uuid,
            region,
            priceDescription,
            benefit1,
            benefit2,
            providedBy
        ) {}
    }
}