package com.monzoprep.feature_insurance.testdata

import com.monzoprep.feature_insurance.view.model.InsuranceView

class InsuranceViewTestDataBuilder {
    companion object {
        fun aInsuranceView(
            uuid: String = "uuid",
            priceType: String = "",
            price: String = "",
            benefit1: String = "",
            benefit1Details: String = "",
            benefit2: String = "",
            benefit2Details: String = "",
            benefit3: String = "",
            providedBy: String = "",
            region: String = ""
        ) = InsuranceView(
            uuid,
            priceType,
            price,
            benefit1,
            benefit1Details,
            benefit2,
            benefit2Details,
            benefit3,
            providedBy,
            region
        )
    }
}