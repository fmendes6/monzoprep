package com.monzoprep.feature_insurance.navigation

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Test

class InsuranceCoordinatorTest {

    private val insuranceNavigator = mock<InsuranceNavigator>()
    private val insuranceCoordinator = InsuranceCoordinator(insuranceNavigator)

    @Test
    fun `should go to insurance details screen when insurance row pressed`() {
        // Arrange
        val insuranceId = "uui"

        // Act
        insuranceCoordinator.insuranceSelected(insuranceId)

        // Assert
        verify(insuranceNavigator).showInsuranceDetails(insuranceId)
    }

    @Test
    fun `should go to dashboard when button pressed`() {
        // Act
        insuranceCoordinator.insuranceDetailButtonPressed()

        // Assert
        verify(insuranceNavigator).goBackToDashboard()
    }
}