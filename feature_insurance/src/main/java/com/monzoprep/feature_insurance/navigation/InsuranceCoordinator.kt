package com.monzoprep.feature_insurance.navigation

internal class InsuranceCoordinator(private val insuranceNavigator: InsuranceNavigator) {

    internal fun insuranceSelected(insuranceId: String) {
        insuranceNavigator.showInsuranceDetails(insuranceId)
    }

    internal fun insuranceDetailButtonPressed() {
        insuranceNavigator.goBackToDashboard()
    }
}