package com.monzoprep.feature_insurance.navigation

import android.app.Activity
import android.content.Intent
import com.monzoprep.core.navigation.NavigationActions
import com.monzoprep.feature_insurance.view.insurance_details.InsuranceDetailActivity

internal class InsuranceNavigator {

    var activity: Activity? = null

    fun goBackToDashboard() = activity?.let {
        openActivity(it, NavigationActions.Dashboard, Intent.FLAG_ACTIVITY_CLEAR_TOP)
    }

    fun showInsuranceDetails(insuranceId: String) = activity?.let {
        val intent = InsuranceDetailActivity.newIntent(it, insuranceId)
        it.startActivity(intent)
    }

    private fun openActivity(activity: Activity, action: String, intentFlags: Int = 0) {
        val intent = Intent(action)
        intent.setPackage(activity.packageName)
        intent.addFlags(intentFlags)
        activity.startActivity(intent)
    }
}