package com.monzoprep.feature_insurance.di

import com.monzoprep.core.di.CoreComponent
import com.monzoprep.core.di.FeatureScope
import com.monzoprep.feature_insurance.view.BaseActivity
import dagger.Component

@Component(
    modules = [InsuranceModule::class],
    dependencies = [CoreComponent::class]
)
@FeatureScope
internal interface InsuranceComponent {
    fun inject(activity: BaseActivity)
}
