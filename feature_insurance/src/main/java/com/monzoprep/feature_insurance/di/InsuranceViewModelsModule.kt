package com.monzoprep.feature_insurance.di

import androidx.lifecycle.ViewModel
import com.monzoprep.core.di.ViewModelKey
import com.monzoprep.feature_insurance.view.choose_insurance.ChooseInsuranceViewModel
import com.monzoprep.feature_insurance.view.insurance_details.InsuranceDetailViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class InsuranceViewModelsModule {

    @Binds
    @IntoMap
    @ViewModelKey(InsuranceDetailViewModel::class)
    internal abstract fun insuranceDetailViewModel(viewModel: InsuranceDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ChooseInsuranceViewModel::class)
    internal abstract fun chooseInsuranceViewModel(viewModel: ChooseInsuranceViewModel): ViewModel
}