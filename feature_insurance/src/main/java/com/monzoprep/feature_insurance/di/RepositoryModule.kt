package com.monzoprep.feature_insurance.di

import android.content.Context
import com.monzoprep.core.di.FeatureScope
import com.monzoprep.feature_insurance.view.InsuranceViewMapper
import com.monzoprep.feature_insurance.view.InsuranceViewMapperImpl
import com.monzoprep.library_domain_repository.UserAssignedServicesRepository
import com.monzoprep.library_domain_repository_room.UserAssignedServicesRepositoryRoom
import com.monzoprep.library_domain_repository_room.dao.UserServicesDao
import com.monzoprep.library_domain_repository_room.database.UserServicesDatabaseWrapper
import com.monzoprep.library_domain_repository_room.mapper.UserServiceMapper
import com.monzoprep.library_insurance_repository.InsuranceRepository
import com.monzoprep.library_insurance_repository_room.InsuranceRepositoryRoom
import com.monzoprep.library_insurance_repository_room.dao.InsurancesDao
import com.monzoprep.library_insurance_repository_room.database.InsuranceDatabaseWrapper
import com.monzoprep.library_insurance_repository_room.mapper.InsuranceMapper
import dagger.Module
import dagger.Provides

@Module
internal object RepositoryModule {

    @JvmStatic
    @FeatureScope
    @Provides
    internal fun provideUserAssignedServicesRepository(
        userServicesDao: UserServicesDao,
        userServiceMapper: UserServiceMapper
    ): UserAssignedServicesRepository {
        return UserAssignedServicesRepositoryRoom(userServicesDao, userServiceMapper)
    }

    @JvmStatic
    @FeatureScope
    @Provides
    internal fun provideUserServicesDao(
        context: Context
    ): UserServicesDao {
        return UserServicesDatabaseWrapper(context).getUserServicesDao()
    }

    @JvmStatic
    @FeatureScope
    @Provides
    internal fun provideUserServiceMapper(): UserServiceMapper {
        return UserServiceMapper()
    }

    @JvmStatic
    @FeatureScope
    @Provides
    internal fun provideInsuranceMapper(): InsuranceMapper {
        return InsuranceMapper()
    }

    @JvmStatic
    @FeatureScope
    @Provides
    internal fun provideInsuranceRepository(
        insurancesDao: InsurancesDao,
        insuranceMapper: InsuranceMapper
    ): InsuranceRepository {
        return InsuranceRepositoryRoom(insurancesDao, insuranceMapper)
    }

    @JvmStatic
    @FeatureScope
    @Provides
    internal fun provideInsurancesDao(
        context: Context
    ): InsurancesDao {
        return InsuranceDatabaseWrapper(context).getInsurancesDao()
    }

    @JvmStatic
    @FeatureScope
    @Provides
    internal fun provideInsuranceViewMapper(context: Context): InsuranceViewMapper {
        return InsuranceViewMapperImpl(context)
    }
}