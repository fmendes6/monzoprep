package com.monzoprep.feature_insurance.di

import com.monzoprep.core.di.FeatureScope
import com.monzoprep.library_domain_repository.UserAssignedServicesRepository
import com.monzoprep.library_domain_usecases.CancelUserAssignedService
import com.monzoprep.library_domain_usecases.SubscribeUserAssignedService
import dagger.Module
import dagger.Provides

@Module
internal object UseCasesModule {

    @JvmStatic
    @FeatureScope
    @Provides
    internal fun provideCancelUserAssignedService(
        userAssignedServicesRepository: UserAssignedServicesRepository
    ): CancelUserAssignedService {
        return CancelUserAssignedService(userAssignedServicesRepository)
    }

    @JvmStatic
    @FeatureScope
    @Provides
    internal fun provideSubscribeUserAssignedService(
        userAssignedServicesRepository: UserAssignedServicesRepository
    ): SubscribeUserAssignedService {
        return SubscribeUserAssignedService(userAssignedServicesRepository)
    }
}