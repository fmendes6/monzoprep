package com.monzoprep.feature_insurance.di

import com.monzoprep.core.di.FeatureScope
import com.monzoprep.core.di.ViewModelFactoryModule
import com.monzoprep.feature_insurance.navigation.InsuranceCoordinator
import com.monzoprep.feature_insurance.navigation.InsuranceNavigator
import dagger.Module
import dagger.Provides

@Module(
    includes = [
        ViewModelFactoryModule::class,
        InsuranceViewModelsModule::class,
        RepositoryModule::class,
        UseCasesModule::class
    ]
)
internal object InsuranceModule {

    @JvmStatic
    @Provides
    @FeatureScope
    internal fun provideNavigator(): InsuranceNavigator {
        return InsuranceNavigator()
    }

    @JvmStatic
    @Provides
    @FeatureScope
    internal fun provideCoordinator(navigator: InsuranceNavigator): InsuranceCoordinator {
        return InsuranceCoordinator(navigator)
    }
}