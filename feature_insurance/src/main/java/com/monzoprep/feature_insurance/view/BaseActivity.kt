package com.monzoprep.feature_insurance.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.monzoprep.core.di.CoreInjectHelper
import com.monzoprep.feature_insurance.di.DaggerInsuranceComponent
import com.monzoprep.feature_insurance.navigation.InsuranceNavigator
import javax.inject.Inject

internal open class BaseActivity : AppCompatActivity() {

    @Inject
    internal lateinit var insuranceNavigator: InsuranceNavigator

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startDi()
        insuranceNavigator.activity = this
    }

    override fun onDestroy() {
        super.onDestroy()
        insuranceNavigator.activity = null
    }

    private fun startDi() {
        DaggerInsuranceComponent
            .builder()
            .coreComponent(CoreInjectHelper.provideCoreComponent(applicationContext))
            .build()
            .inject(this)
    }
}