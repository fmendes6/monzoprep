package com.monzoprep.feature_insurance.view.model

data class InsuranceView(
    val uuid: String,
    val priceType: String,
    val price: String,
    val benefit1: String,
    val benefit1Details: String,
    val benefit2: String,
    val benefit2Details: String,
    val benefit3: String,
    val providedBy: String,
    val region: String
)