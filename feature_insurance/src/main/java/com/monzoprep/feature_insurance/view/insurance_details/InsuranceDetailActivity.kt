package com.monzoprep.feature_insurance.view.insurance_details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.monzoprep.core.view.IntentKeys
import com.monzoprep.feature_insurance.R
import com.monzoprep.feature_insurance.view.BaseActivity
import com.monzoprep.feature_insurance.view.model.InsuranceView
import kotlinx.android.synthetic.main.activity_insurance_detail.*

internal class InsuranceDetailActivity : BaseActivity() {

    private lateinit var viewModel: InsuranceDetailViewModel
    private var isSubscribed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_insurance_detail)
        setSupportActionBar(insurance_details_toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val insuranceId = intent.getStringExtra(IntentKeys.InsuranceDetailInsuraceId)!!
        isSubscribed = intent.getBooleanExtra(IntentKeys.InsuranceDetailIsInsuraceEnabled, false)

        viewModel = ViewModelProviders.of(this, viewModelFactory)[InsuranceDetailViewModel::class.java]
        viewModel.getInsurance(insuranceId)

        insurance_confirm_button.setOnClickListener {
            viewModel.confirmButtonPressed(isSubscribed, insuranceId)
        }

        viewModel.viewState.observe(this, Observer { state ->
            fillScreen(state)
        })
    }

    private fun fillScreen(insuranceView: InsuranceView) {
        val colorId =
            if (isSubscribed) R.color.cancel_button_background_color else R.color.confirm_button_background_color
        val textResId = if (isSubscribed) R.string.insurance_cancel else R.string.insurance_subscribe
        val color: Int = ContextCompat.getColor(this, colorId)

        insurance_provided.text = insuranceView.providedBy
        insurance_cost.text = insuranceView.price
        insurance_type.text = insuranceView.priceType
        insurance_benefit1.text = insuranceView.benefit1
        insurance_benefit2.text = insuranceView.benefit2
        insurance_benefit3.text = insuranceView.benefit3
        insurance_benefit1_sub.text = insuranceView.benefit1Details
        insurance_benefit2_sub.text = insuranceView.benefit2Details
        insurance_region.text = insuranceView.region
        insurance_confirm_button.setBackgroundColor(color)
        insurance_confirm_button.text = getString(textResId)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    companion object {
        fun newIntent(context: Context, insuranceId: String, isSubscribed: Boolean = false): Intent {
            val intent = Intent(context, InsuranceDetailActivity::class.java)
            intent.putExtra(IntentKeys.InsuranceDetailInsuraceId, insuranceId)
            intent.putExtra(IntentKeys.InsuranceDetailIsInsuraceEnabled, isSubscribed)
            return intent
        }
    }
}
