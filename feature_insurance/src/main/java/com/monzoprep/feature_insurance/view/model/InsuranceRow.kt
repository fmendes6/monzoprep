package com.monzoprep.feature_insurance.view.model

import com.monzoprep.feature_insurance.R
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.view_insurance_row.*

class InsuranceRow(
    private val uuid: String,
    internal val region: String,
    private val priceDescription: String,
    private val benefit1: String,
    private val benefit2: String,
    private val providedBy: String,
    private val insuranceClicked: (String) -> Unit
) : Item() {

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.insurance_option.setOnClickListener { insuranceClicked(uuid) }
        viewHolder.insurance_row_price.text = priceDescription
        viewHolder.insurance_row_benefit1.text = benefit1
        viewHolder.insurance_row_benefit2.text = benefit2
        viewHolder.insurance_row_provided.text = providedBy
    }

    override fun getLayout() = R.layout.view_insurance_row
}