package com.monzoprep.feature_insurance.view

import com.monzoprep.domain.models.insurance.Insurance
import com.monzoprep.feature_insurance.view.model.InsuranceView
import com.xwray.groupie.kotlinandroidextensions.Item

internal interface InsuranceViewMapper {
    fun mapToDetails(insurance: Insurance): InsuranceView
    fun mapToItems(insurances: List<Insurance>, itemClick: (String) -> Unit): List<Item>
    fun mapToItem(insurance: Insurance, itemClick: (String) -> Unit): Item
}