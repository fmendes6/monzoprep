package com.monzoprep.feature_insurance.view.insurance_details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.monzoprep.domain.models.UserService
import com.monzoprep.domain.models.UserServiceType
import com.monzoprep.feature_insurance.navigation.InsuranceCoordinator
import com.monzoprep.feature_insurance.view.InsuranceViewMapper
import com.monzoprep.feature_insurance.view.model.InsuranceView
import com.monzoprep.library_domain_usecases.CancelUserAssignedService
import com.monzoprep.library_domain_usecases.SubscribeUserAssignedService
import com.monzoprep.library_insurance_repository.InsuranceRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch
import javax.inject.Inject

internal class InsuranceDetailViewModel @Inject constructor(
    private val insuranceCoordinator: InsuranceCoordinator,
    private val insuranceViewMapper: InsuranceViewMapper,
    private val insuranceRepository: InsuranceRepository,
    private val cancelUserAssignedService: CancelUserAssignedService,
    private val subscribeUserAssignedService: SubscribeUserAssignedService
) : ViewModel() {

    private lateinit var subscription: Disposable
    val viewState: MutableLiveData<InsuranceView> by lazy {
        MutableLiveData<InsuranceView>()
    }

    fun getInsurance(uuid: String) {
        subscription = insuranceRepository.getInsuranceById(uuid)
            .map { insuranceViewMapper.mapToDetails(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(Consumer {
                viewState.value = it
            })
    }

    fun confirmButtonPressed(hasInsurance: Boolean, insuranceUuid: String) {
        val userService = UserService(UserServiceType.MEDICAL_INSURANCE, insuranceUuid)

        viewModelScope.launch {
            updateUserService(hasInsurance, userService)
            insuranceCoordinator.insuranceDetailButtonPressed()
        }
    }

    private suspend fun updateUserService(hasInsurance: Boolean, userService: UserService) {
        if (hasInsurance) {
            cancelUserAssignedService.cancel(userService)
        } else {
            subscribeUserAssignedService.subscribe(userService)
        }
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }
}