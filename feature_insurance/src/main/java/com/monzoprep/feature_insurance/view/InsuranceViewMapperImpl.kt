package com.monzoprep.feature_insurance.view

import android.content.Context
import com.monzoprep.domain.models.insurance.Insurance
import com.monzoprep.domain.models.insurance.InsuranceRegion
import com.monzoprep.domain.models.insurance.InsuranceType
import com.monzoprep.feature_insurance.R
import com.monzoprep.feature_insurance.view.model.InsuranceRow
import com.monzoprep.feature_insurance.view.model.InsuranceView
import com.xwray.groupie.kotlinandroidextensions.Item

internal class InsuranceViewMapperImpl(private val context: Context) : InsuranceViewMapper {

    override fun mapToItems(
        insurances: List<Insurance>,
        itemClick: (String) -> Unit
    ): List<Item> {
        return insurances.map { mapToItem(it, itemClick) }
    }

    override fun mapToItem(insurance: Insurance, itemClick: (String) -> Unit): Item {
        val cost = getCost(insurance)
        val region = getRegion(insurance.region)
        return InsuranceRow(
            insurance.uuid,
            region,
            cost,
            insurance.benefit1,
            insurance.benefit2,
            insurance.providedBy,
            itemClick
        )
    }

    override fun mapToDetails(insurance: Insurance): InsuranceView {
        val paymentType = getPaymentTypeForDetails(insurance.insuranceType)
        val region = getRegion(insurance.region)
        val benefit1Details = context.getString(R.string.insurance_benefit_details1)
        val benefit2Details = context.getString(R.string.insurance_benefit_details2)

        return InsuranceView(
            insurance.uuid,
            paymentType,
            insurance.cost.toString(),
            insurance.benefit1,
            benefit1Details,
            insurance.benefit2,
            benefit2Details,
            insurance.benefit3,
            insurance.providedBy,
            region
        )
    }

    private fun getCost(insurance: Insurance): String {
        val paymentType = getPaymentType(insurance.insuranceType)
        return context.getString(
            R.string.insurance_row_price_partial_text,
            insurance.cost.toString(),
            paymentType
        )
    }

    private fun getPaymentType(insuranceType: InsuranceType): String {
        return context.getString(
            when (insuranceType) {
                InsuranceType.PayPerDay -> R.string.insurance_row_type_day_text
                InsuranceType.Monthly -> R.string.insurance_row_type_month_text
            }
        )
    }

    private fun getPaymentTypeForDetails(insuranceType: InsuranceType): String {
        return context.getString(
            when (insuranceType) {
                InsuranceType.PayPerDay -> R.string.insurance_payperday
                InsuranceType.Monthly -> R.string.insurance_monthly
            }
        )
    }

    private fun getRegion(insuranceRegion: InsuranceRegion): String {
        return context.getString(
            when (insuranceRegion) {
                InsuranceRegion.Europe -> R.string.insurance_continent_europe_label
                InsuranceRegion.NorthAmerica -> R.string.insurance_continent_na_label
                InsuranceRegion.RestOfWorld -> R.string.insurance_continent_world_label
            }
        )
    }
}