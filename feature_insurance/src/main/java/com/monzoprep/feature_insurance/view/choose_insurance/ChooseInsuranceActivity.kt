package com.monzoprep.feature_insurance.view.choose_insurance

import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.monzoprep.feature_insurance.R
import com.monzoprep.feature_insurance.view.BaseActivity
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import com.xwray.groupie.kotlinandroidextensions.Item
import kotlinx.android.synthetic.main.activity_choose_insurance.*

internal class ChooseInsuranceActivity : BaseActivity() {

    private lateinit var viewModel: ChooseInsuranceViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_insurance)
        setSupportActionBar(insurance_choose_toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        viewModel = ViewModelProviders.of(this, viewModelFactory)[ChooseInsuranceViewModel::class.java]
        viewModel.getInsurances()
        viewModel.viewState.observe(this, Observer { state ->
            setAdapterWith(state)
        })
    }

    private fun setAdapterWith(insurances: List<Item>) {

        val viewManager = LinearLayoutManager(this)
        val itemDecoration = DividerItemDecoration(this, viewManager.orientation)

        val groupAdapter = GroupAdapter<ViewHolder>()
        groupAdapter.addAll(insurances)

        insurance_choose_recyclerview.apply {
            setHasFixedSize(true)
            addItemDecoration(itemDecoration)
            layoutManager = viewManager
            adapter = groupAdapter
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }
}
