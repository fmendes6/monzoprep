package com.monzoprep.feature_insurance.view.model

import com.monzoprep.feature_insurance.R
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.view_insurance_row_header.*

class InsuranceRowHeader(private val region: String) : Item() {

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.account_borrowing_label.text = region
    }

    override fun getLayout() = R.layout.view_insurance_row_header
}