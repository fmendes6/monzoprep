package com.monzoprep.feature_insurance.view.choose_insurance

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.monzoprep.feature_insurance.navigation.InsuranceCoordinator
import com.monzoprep.feature_insurance.view.InsuranceViewMapper
import com.monzoprep.feature_insurance.view.model.InsuranceRow
import com.monzoprep.feature_insurance.view.model.InsuranceRowHeader
import com.monzoprep.library_insurance_repository.InsuranceRepository
import com.xwray.groupie.kotlinandroidextensions.Item
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

internal class ChooseInsuranceViewModel @Inject constructor(
    private val insuranceCoordinator: InsuranceCoordinator,
    private val insuranceViewMapper: InsuranceViewMapper,
    private val insuranceRepository: InsuranceRepository
) : ViewModel() {

    private lateinit var subscription: Disposable

    val viewState: MutableLiveData<List<Item>> by lazy {
        MutableLiveData<List<Item>>()
    }

    fun insuranceSelected(insuranceId: String) = insuranceCoordinator.insuranceSelected(insuranceId)

    fun getInsurances() {
        subscription = insuranceRepository.getAllInsurances()
            .defaultIfEmpty(listOf())
            .map { it.sortedBy { insurance -> insurance.region.ordinal } }
            .map { insuranceViewMapper.mapToItems(it, this::insuranceSelected) }
            .map { addHeaders(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { viewState.value = it },
                { viewState.value = listOf() })
    }

    private fun addHeaders(insurances: List<Item>): List<Item> {
        val views = mutableListOf<Item>()
        for (insurance in insurances) {
            val region = (insurance as InsuranceRow).region

            if (views.withoutHeader(region)) {
                views.add(InsuranceRowHeader(region))
            }
            views.add(insurance)
        }
        return views
    }

    private fun MutableList<Item>.withoutHeader(region: String): Boolean {
        return isEmpty() || isNotEmpty() && (this.last() as InsuranceRow).region != region
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }
}
