package com.monzoprep.library_domain_repository

import com.monzoprep.domain.models.UserService
import com.monzoprep.domain.models.UserServiceType

interface UserAssignedServicesRepository {
    suspend fun getService(userServiceType: UserServiceType): UserService
    suspend fun enableService(userService: UserService)
    suspend fun disableService(userService: UserService)
}